const mongoose = require("mongoose");

const NewsSchema = mongoose.Schema({
  title: {
    type: String,
    required: [true, "A news must have a title"],
    maxlength: 50,
  },
  description: {
    type: String,
    required: [true, " A news must have a description"],
  },
  coverImage: {
    type: String,
    default: "no-img.png",
  },
  featured: {
    type: Boolean,
    default: false,
  },
  lastUpdatedAt: {
    type: Date,
    default: undefined,
  },
  createdAt: {
    type: Date,
    default: Date.now(),
  },
  category: {
    type: mongoose.Schema.ObjectId,
    ref: "Category",
    required: [true, "Please Provide the category for the news"],
  },
  author: {
    type: mongoose.Schema.ObjectId,
    ref: "User",
    required: [true, "A news must have a valid autor"],
  },
});

module.exports = mongoose.model("News", NewsSchema);
