const mongoose = require("mongoose");

const CategorySchema = mongoose.Schema(
  {
    title: {
      type: String,
      required: true,
      enum: ["Agriculture", "Health", "Sports", "Politics"],
    },
  },
  {
    toJSON: { virtuals: true },
    toObject: { virtuals: true },
  }
);

// Setting Category to Undefined in case the category gets Deleted
CategorySchema.pre("remove", async function (next) {
  console.log("News Category being Set to null/Undefined");
  const category = undefined;
  const categoryNews = await this.model("News").find();
  categoryNews.forEach(async (news) => {
    await this.model("News").findByIdAndUpdate(news._id, {
      category,
    });
  });
  next();
});

CategorySchema.virtual("news", {
  ref: "News",
  localField: "_id",
  foreignField: "category",
  justOne: false,
});

module.exports = mongoose.model("Category", CategorySchema);
