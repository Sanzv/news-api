const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const UserSchema = mongoose.Schema({
  name: {
    type: String,
    required: [true, "Please add a name"],
  },
  email: {
    type: String,
    required: [true, "Please add a valid email"],
    unique: [true, "Email already Exists"],
    match: [
      /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
      "Please add valid email",
    ],
  },
  password: {
    type: String,
    required: [true, "Please provide a Password"],
    minlength: 8,
    maxlength: 15,
    select: false,
  },
  role: {
    type: String,
    required: true,
    enum: ["admin", "user", "publisher"],
  },
  createadAt: {
    type: Date,
    default: Date.now(),
  },
});

// Encrypt Password Before save
UserSchema.pre("save", async function (next) {
  const salt = await bcrypt.genSalt(10);
  this.password = await bcrypt.hash(this.password, salt);
  next();
});

// Register a method to the user module to generate json web token
UserSchema.methods.getSignedToken = function () {
  const payload = { id: this._id };
  return jwt.sign(payload, process.env.JWT_SECRET, {
    expiresIn: process.env.JWT_EXPIRE,
  });
};

// To match th password with hashed password
UserSchema.methods.matchPassword = async function (enteredPassword) {
  return await bcrypt.compare(enteredPassword, this.password);
};

module.exports = mongoose.model("User", UserSchema);
