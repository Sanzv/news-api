// Loading the packages
const express = require("express");
const dotenv = require("dotenv");
const morgan = require("morgan");
const cookieParser = require("cookie-parser");
const multer = require("multer");

// Importing self built utils, middlewares and routers
const connectDB = require("./utils/db");
const authRouter = require("./routes/authRoute");
const newsRouter = require("./routes/newsRoute");
const categoryRouter = require("./routes/categoryRoute");

const errorHandler = require("./middlewares/ErrorHandler");

// Load environment variables
dotenv.config({ path: "./config/config.env" });

// Initialize express app
const app = express();

// Connecting to the Database
connectDB();

// Use several Middlewares
app.use(express.json()); // Parsing request Body
app.use(cookieParser()); // Parsing Cookie

// Mount Routers
app.use("/auth", authRouter);
app.use("/news", newsRouter);
app.use("/category", categoryRouter);

app.use(errorHandler); // Custom Error Handling Middleware

app.use(express.static("public"));

// Listen to the server
const server = app.listen(
  process.env.PORT || 5000,
  console.log(`Listening to the server at port ${process.env.PORT || 5000}`)
);

// Handling unhandled Promise Rejection
process.on("unhandledRejection", (err, promise) => {
  console.log(`Error ${err.message.toString()} Occured.\n Exiting Server`);

  // Close server and Exit
  server.close(() => process.exit(1));
});
