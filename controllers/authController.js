const User = require("../models/UserModel");
const jwt = require("jsonwebtoken");
const asyncHandler = require("../middlewares/async");

exports.getUsers = asyncHandler(async (req, res, next) => {
  const users = await User.find();
  res.status(200).json({ success: true, count: users.length, data: users });
});

exports.register = asyncHandler(async (req, res, next) => {
  const { name, email, password, role } = req.body;

  const user = await User.create({
    name,
    email,
    role,
    password,
  });
  sendTokenResponse(user, 200, res, "Signed Up SuccessFully");
});

exports.login = asyncHandler(async (req, res, next) => {
  const { email, password } = req.body;
  if (!email || !password) {
    return next(
      new ErrorResponse(`Please Provide both Email and password`),
      403
    );
  }
  const user = await User.findOne({ email }).select("+password");
  if (!user) {
    return next(new ErrorResponse(`Email Not registered`), 404);
  }
  const isMatch = await user.matchPassword(password);
  if (!isMatch) {
    return next(new ErrorResponse(`Password Incorrect`), 401);
  }
  sendTokenResponse(user, 200, res, "Log In successful");
});

exports.deleteUser = asyncHandler(async (req, res, next) => {
  const user = await User.findById(req.params.id);
  if (!user) {
    return next(
      new ErrorResponse(`Cannot Find the User with id ${req.params.id}`),
      404
    );
  }
  await user.remove();
  res.status(200).json({ success: true, message: "User deleted Successfully" });
});

exports.currentUser = asyncHandler(async (req, res, next) => {
  const user = req.user;
  res.status(200).json({ success: true, data: user });
});

const sendTokenResponse = (user, statusCode, res, message = "") => {
  const options = {
    expires: new Date(
      Date.now() + process.env.JWT_COOKIE_EXPIRE * 24 * 60 * 60 * 1000
    ),
    httpOnly: true,
  };

  const token = user.getSignedToken();

  res
    .status(statusCode)
    .cookie("token", token, options)
    .json({ success: true, token, message });
};
