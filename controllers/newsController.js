const News = require("../models/NewsModel");
const asyncHandler = require("../middlewares/async");
const ErrorResponse = require("../utils/ErrorResponse");
const path = require("path");



// Getting all News
exports.getNews = asyncHandler(async (req, res, next) => {
  let options = {};
  if (req.params.id) {
    options = { category: req.params.id };
  }
  const news = await News.find(options).populate("category");
  res.status(200).json({ success: true, count: news.length, data: news });
});

// Getting Single News
exports.getSingleNews = asyncHandler(async (req, res, next) => {
  const news = await News.findById(req.params.id).populate("category");
  if (!news) {
    return next(
      new ErrorResponse(`Cannot Find the News with id ${req.params.id}`),
      404
    );
  }
  res.status(200).json({ success: true, data: news });
});

// Creating News
exports.createNews = asyncHandler(async (req, res, next) => {
  console.log(req.files);
  req.body.author = req.user.id;
  const news = await News.create(req.body);
  res.status(200).json({ success: true, data: news });
});

// Updating News
exports.updateNews = asyncHandler(async (req, res, next) => {
  try {
    let news = await News.findById(req.params.id);
    if (!news) {
      return res.status(404).json({
        success: false,
        message: `Cannot Find the News with id ${req.params.id}`,
      });
    }
    if (news.author != req.user.id && req.user.role !== "admin") {
      console.log(req.user.id);
      return res.status(401).json({
        success: false,
        message: "You must be the author or Admin to update the news",
      });
    }
    news = await News.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
      runValidators: true,
    });
    res.status(200).json({ success: true, data: news });
  } catch (err) {
    res.status(500).json({ success: false, message: "Cannot Update News" });
  }
});

// Deleting News
exports.deleteNews = asyncHandler(async (req, res, next) => {
  try {
    const news = await News.findById(req.params.id);
    if (!news) {
      return res.status(404).json({
        success: false,
        message: `Cannot Find the News with id ${req.params.id}`,
      });
    }
    if (news.author != req.user.id && req.user.role !== "admin") {
      console.log(req.user.id);
      return res.status(401).json({
        success: false,
        message: "You must be the author or Admin to delete the news",
      });
    }
    news.remove();
    res.status(200).json({ success: true });
  } catch (err) {
    res.status(500).json({ success: false, message: "Cannot Delete News" });
  }
});
