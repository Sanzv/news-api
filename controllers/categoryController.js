const Category = require("../models/CategoryModel");
const asyncHandler = require("../middlewares/async");

// Getting all Categories
exports.getCategories = asyncHandler(async (req, res, next) => {
  const categories = await Category.find({}).populate("news");
  res
    .status(200)
    .json({ success: true, count: categories.length, data: categories });
});

// Creating Category
exports.createCategory = asyncHandler(async (req, res, next) => {
  if (!req.body.title) {
    return next(new ErrorResponse(`Please Provide Title`), 400);
  }
  const category = await Category.create(req.body);
  res.status(200).json({ success: true, data: category });
});

// Deleting Category
exports.deleteCategory = asyncHandler(async (req, res, next) => {
  const category = await Category.findById(req.params.id);
  if (!category) {
    return next(
      new ErrorResponse(`Cannot Find the Category with id ${req.params.id}`),
      404
    );
  }
  await Category.findByIdAndDelete(req.params.id);
  res
    .status(200)
    .json({ success: true, message: " Category Deleted Successfully" });
});
