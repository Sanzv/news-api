const express = require("express");
const { upload } = require("../middlewares/imageUploader");
const router = express.Router({ mergeParams: true });

const { protect, authorize } = require("../middlewares/auth");

const {
  getNews,
  createNews,
  deleteNews,
  updateNews,
  getSingleNews,
} = require("../controllers/newsController");

router
  .route("/")
  .get(getNews)
  .post(protect, authorize("admin", "publisher"), createNews);
router
  .route("/:id")
  .put(protect, authorize("admin", "publisher"), updateNews)
  .delete(protect, authorize("admin", "publisher"), deleteNews)
  .get(getSingleNews);

module.exports = router;
