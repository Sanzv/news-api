const express = require("express");

const {
  getUsers,
  register,
  login,
  deleteUser,
  currentUser,
} = require("../controllers/authController");

const { protect, authorize } = require("../middlewares/auth");

const router = express.Router();

router.route("/users").get(getUsers);
router.route("/register").post(register);
router.route("/login").post(login);
router.route("/me").get(protect, currentUser);
router.route("/users/:id").delete(protect, authorize("admin"), deleteUser);

module.exports = router;
