const express = require("express");

const newsRouter = require("./newsRoute");

const router = express.Router();
const { protect, authorize } = require("../middlewares/auth");

const {
  getCategories,
  createCategory,
  deleteCategory,
} = require("../controllers/categoryController");

router.get("/", getCategories);
router.post("/", protect, authorize("admin", "publisher"), createCategory);
router.delete("/:id", protect, authorize("admin", "publisher"), deleteCategory);

router.use("/:id/news", newsRouter);

module.exports = router;
