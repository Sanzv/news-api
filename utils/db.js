const mongoose = require("mongoose");

// Connecting to the database
const connectDB = async () => {
  const con = await mongoose.connect(process.env.DB_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: true,
  });
  console.log(`Connected to the database ${con.connection.host}`);
};

module.exports = connectDB;
