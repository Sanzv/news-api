const ErrorResponse = require("../utils/ErrorResponse");

const errorHandler = (err, req, res, next) => {
  let error = { ...err };
  console.log(`Error is ${error}`);
  console.log(err);
  error.message = err.message;

  // Mongoose Validation Error
  if (err.name === "ValidationError") {
    const message = Object.values(err.errors).map((val) => val.message);
    error = new ErrorResponse(message, 400);
  }
  // MOngoose Resource not found error
  if (err.name === "CastError") {
    const message = `Cannot Find Resource with ID ${err.value}`;
    error = new ErrorResponse(message, 404);
  }
  // MOngoose Duplicate Key error
  if (err.code === "11000") {
    const message = `Duplicate field value Entered`;
    error = new ErrorResponse(message, 400);
  }
  res
    .status(error.statusCode || 500)
    .json({ success: false, message: error.message || "Server Error" });
};

module.exports = errorHandler;
