const jwt = require("jsonwebtoken");
const User = require("../models/UserModel");
const ErrorResponse = require("../utils/ErrorResponse");

exports.protect = async (req, res, next) => {
  let token;

  if (!req.headers.authorization) {
    return next(
      new ErrorResponse(
        "Did not find any token for authorization. Please Log in To continue",
        403
      )
    );
  }

  if (
    req.headers.authorization &&
    req.headers.authorization.startsWith("Bearer")
  ) {
    token = req.headers.authorization.split(" ")[1];
  }

  if (!token) {
    return next(
      new ErrorResponse(
        "Did not find any token for authorization. Please Log in To continue",
        403
      )
    );
  }

  try {
    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    const user = await User.findById(decoded.id);
    if (!user) {
      return next(
        new ErrorResponse(
          "Did not find any User Associated with the token",
          404
        )
      );
    }
    req.user = user;
    next();
  } catch (err) {
    return next(new ErrorResponse("Token Verification failed", 403));
  }
};

exports.authorize = (...roles) => {
  return (req, res, next) => {
    if (!roles.includes(req.user.role)) {
      next(
        new ErrorResponse(
          ` User with role ${req.user.role} is not allowed to perform this`,
          403
        )
      );
    }
    next();
  };
};
