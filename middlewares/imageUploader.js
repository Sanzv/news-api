const multer = require("multer");

// For setting up uploading Images

exports.upload = (req, res, next) => {
  const imageMimeTypes = ["image/png", "image/jpeg", "images/gif"];
  const uploadPath = "images/public/news";

  const storage = multer.diskStorage({
    destination: (req, file, callback) => {
      callback(null, uploadPath);
    },
    filename: (req, file, callback) => {
      callback(null, file.filename + "-" + Date.now());
    },
    filefilter: (req, file, callback) => {
      callback(null, imageMimeTypes.includes(file.mimetype));
    },
  });

  const upload = multer({ storage }).array("images", 10);
  upload(req, res, function (err) {
    if (err instanceof multer.MulterError) {
      next(new ErrorResponse("Cannot Upload Files", 500));
    } else {
      next();
    }
  });
  next();
};
